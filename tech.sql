-- MySQL dump 10.16  Distrib 10.1.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: tech
-- ------------------------------------------------------
-- Server version	10.1.44-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `full_description` longtext COLLATE utf8mb4_unicode_ci,
  `type_id` int(11) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FEC530A9C54C8C93` (`type_id`),
  KEY `IDX_FEC530A984A0A3ED` (`content_id`),
  CONSTRAINT `FK_FEC530A984A0A3ED` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`),
  CONSTRAINT `FK_FEC530A9C54C8C93` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (18,'NOS EXPERIENCES',NULL,NULL,NULL,NULL,9,NULL,NULL,NULL),(19,'Angular',NULL,NULL,'80',NULL,NULL,18,NULL,NULL),(20,'PHP',NULL,NULL,'97',NULL,NULL,18,NULL,NULL),(24,'Qui sommes-nous ?','i',NULL,'Spécialisés dans l’ingénierie des technologies de pointe, nous disposons des compétences et ressources nécessaires pour vous accompagner dans votre transformation numérique.\r\nUn projet d\'externalisation informatique ? Nous sommes votre interlocuteur privilégié.',NULL,3,NULL,NULL,NULL),(26,'A votre service',NULL,NULL,'Notre mission : réaliser des solutions sur mesure pour accélérer votre évolution et vous faire gagner en efficacité pour votre développement digital.',NULL,4,NULL,NULL,NULL),(28,'Mobile','Lorem Lorem ipsum','64541-development-web-shopping-business-e-commerce-design-online-5f4679c7b2061.png','Web et e-commerce','Conception et développement de site web : site professionnel, site vitrine, site e-commerce sur mesure (prestashop, magento marketplace, drupal, wordpress…), site spécifique… | Intégration sur mobile, desktop et site responsive design | Création de visuel : charte graphique, logo, UI/UX…',NULL,26,NULL,NULL),(29,'Ampoule','Lorem dolor dolor','73692-e-commerce-smartphone-telephone-iphone-download-hq-png-5f4679d981868.png','Applications mobiles','Conception et développement mobile : Android, iOS et Windows Phone | Applications natives ou hybrides, BtoC et BtoB couplées d’un système d’information | Interfaces ergonomiques et intuitives avec des fonctionnalités mobiles innovantes : QR Code NFC…',NULL,26,NULL,NULL),(30,'DATA SCIENCE & IA',NULL,'Untitled-1-5f452175bad6a.png','azerazeareezre',NULL,NULL,24,NULL,NULL),(31,'GAMING/ANIMATION 2D-3D',NULL,NULL,'azer',NULL,NULL,24,NULL,NULL),(32,'BUSINESS INTELLIGENCE',NULL,NULL,NULL,NULL,NULL,24,NULL,NULL),(33,'Création graphique',NULL,NULL,NULL,NULL,5,NULL,NULL,NULL),(34,'AFFICHES / PLAQUETTES / BROCHURES / CATALOGUES',NULL,'affiche-voyage-paumes-23-2148043322-5f2980591e89f.jpeg','L’affiche est l’outil de communication le plus important et facilite la communication avec le grand public. Il est idéal pour présenter un événement, un nouveau produit ou un nouveau concept. Création d’une brochure pour un produit et une entreprise. Que ce soit le format d’affiche que vous souhaitez présenter avec des outils de publicité et de communication pour mettre en valeur votre activité ou vos produits, notre graphiste est assuré de la personnalisation de votre campagne. Le panneau “4 sur 3”, roll-up, une simple feuille A4 où un multiformat, vinyle, étiquettes, goodies publicitaires, packaging, habillage stands, panneaux, enseignes, Covering véhicule, ….',NULL,NULL,33,NULL,NULL),(35,'LOGOTYPES',NULL,'logo-5f2980eb2f69a.png','Le logotype est un élément important et la base de toute identité visuelle. C’est un élément graphique composé d’une forme, d’un objet, d’une couleur qui permet d’exprimer ses valeurs d’entreprise où ses produits et contribue en effet à l’image de marque. Le logotype est un symbole identifiant une structure, une institution, une entreprise ou une marque. Tech-no-limit s’engage à créer avec vous un logo unique à l’image de votre entreprise et à décliner selon les règles de la communication pour une parfaite adaptation aux différents supports.',NULL,NULL,33,NULL,NULL),(36,'WEB DESIGN',NULL,'webdesign-petit-1-5f298167123f8.jpeg','Conception de sites Web uniques de tous types tels que vitrine, blog, commerce électronique, réseau social, etc.\r\nUn design unique est un élément important de la stratégie de marque et de marketing de votre entreprise.\r\nLe site Web est un outil de communication qui doit répondre à des critères spécifiques en matière de conception ou d’ergonomie et doit également offrir une expérience utilisateur forte.',NULL,NULL,33,NULL,NULL),(37,'CARTE DE VISITE / FLYERS / DÉPLIANTS',NULL,'flyer-5f2982a172fb7.jpeg','La carte de visite a plusieurs fonctions: elle consiste principalement à présenter vos informations de contact et il s’agit également de votre première publicité.\r\nFlyers et dépliant pour Institutionnel ou pour la publicité, ce sont des outils et des moyens de communication pour présenter un produit, une marque ou une activité: restaurant, magasin, commerce ou association et aussi sur un événement commercial, culturel ou promotionnel: vente flash, promotionnelle offre sur un produit, etc …',NULL,NULL,33,NULL,NULL),(38,'SITE INTERNET • MOBILE • RÉFÉRENCEMENT • DOMAINE • HÉBERGEMENT • SUPPORT',NULL,'design-5f2979b19807c.png','Tout y est !',NULL,7,NULL,NULL,NULL),(39,'Web et E-commerce',NULL,'cms-developers-banner-1-5f297a84c1818.png','Développement et intégration avec les CMS Open Source : WordPress, Joomla, Prestashop,\r\n... \r\nCompatibilité, performance et rentabilité. Développez et personnalisez un thème puissant et sécurisé sur WordPress avec le meilleur framework Genesis. Et comme Gantry Framework pour Joomla. Profitez d’un soutien illimité sur votre projet et mettez à jour sur votre propre site en un clic.',NULL,NULL,38,'#34AFC2','white'),(40,'Applications mobiles',NULL,'app-mobile3-5f297d52d7979.jpeg','Concevez des applications mobiles pour tout type d’activité. Applications mobiles utilisant un framework ionique. Compatible et hybride pour les systèmes d’exploitation comme Android et Ios (Apple). spécialisée dans la transformation digitale vous accompagne dans la conception et développements d’applications mobiles que ce soit en langage Natif, Objective C et Java Android, ou en HTML... Quel que soit vos besoins, Tech-no-limit saura imaginer les fonctionnalités, l\'ergonomie et le design qui assureront le succès de votre application mobile.',NULL,NULL,38,NULL,NULL),(43,'LES TECHNOLOGIES UTILISEES',NULL,NULL,NULL,NULL,8,NULL,NULL,NULL),(44,NULL,NULL,'5847ea22cef1014c0b5e4833-5f48c532e4146.png',NULL,NULL,NULL,43,NULL,NULL),(45,NULL,NULL,'PHP-01-5f476cb888215.png',NULL,NULL,NULL,43,NULL,NULL),(47,NULL,NULL,'Java-01-5f476c5a4094d.png',NULL,NULL,NULL,43,NULL,NULL),(49,NULL,NULL,'Oracle-01-5f476d0bc06df.png',NULL,NULL,NULL,43,NULL,NULL),(50,'Gestion des slides',NULL,NULL,'lorem ipsum dolor hamet',NULL,10,NULL,NULL,NULL),(55,'Web et e-commerce','test','outils-metiers2-5f296cd92dbed-5f4688a623b6f.png','Outils métiers','Conception et développement d’outils métiers génériques et spécifiques selon les demandes du client | Solutions pour des outils collaboratifs, accessibilité permanente, outils évolutifs et synchrones à ceux existants | Modules personnalisés pour le CRM, l’intranet…',NULL,26,NULL,NULL),(56,'Symfony',NULL,NULL,'90',NULL,NULL,18,NULL,NULL),(57,'Java',NULL,NULL,'77',NULL,NULL,18,NULL,NULL),(58,'React JS',NULL,NULL,'70',NULL,NULL,18,NULL,NULL),(59,'Ionic',NULL,NULL,'80',NULL,NULL,18,NULL,NULL),(60,'Android',NULL,NULL,'85',NULL,NULL,18,NULL,NULL),(61,'Objective-C',NULL,NULL,'70',NULL,NULL,18,NULL,NULL),(62,'Oracle',NULL,NULL,'80',NULL,NULL,18,NULL,NULL),(63,'Postgresql',NULL,NULL,'70',NULL,NULL,18,NULL,NULL),(64,'Mysql',NULL,NULL,'90',NULL,NULL,18,NULL,NULL),(66,'MongoDB',NULL,NULL,'60',NULL,NULL,18,NULL,NULL),(69,'Des développements sur mesure',NULL,'sur-mesure-1024x652-5f29a0e153e08.jpeg','Un métier en créant pour vous des outils uniques dédiés à vos activités. Votre demande est analysée et développée sur une plateforme sur mesure basée sur les frameworks de développement les plus puissants tels que Laravel, Symfony.\r\nLogiciels sur mesure de gestion d’entreprise.',NULL,NULL,38,'#34C28E','white'),(70,NULL,NULL,'Node-JS-01-5f476d43bfc70.png',NULL,NULL,NULL,43,NULL,NULL),(71,NULL,NULL,'MySQL-01-5f476dc3e87be.png',NULL,NULL,NULL,43,NULL,NULL),(72,'A propos de nous',NULL,NULL,'Tech-no-limit est un réseau de compétences composé de graphistes et de développeurs. Notre mission est d’atteindre votre objectif et de gagner votre confiance. Nous développons des plateformes Web performantes et des logiciels sur mesure pour maximiser la croissance des entreprises qui ont le numérique au coeur de leur modèle d’affaires. Nous sommes donc prêts à assumer la responsabilité de votre projet dans le domaine de l’Internet avec une assistance 24/7.\r\n\r\nCréation Site internet (Vitrine, Blog, …) et Web Application & Mobile Application, E-comerce (boutique en ligne), logiciels sur mesure de gestion d’entreprise. Création de logo pour votre entreprise ou projet.\r\n\r\nEt d’autre service avec l’une des imaginations à exploiter pour avoir votre visuel unique et facile à transférer le message à votre client.\r\n\r\nCréation de flyers, affiches … tous les types de design sur mesure.',NULL,11,NULL,NULL,NULL),(73,'#Caracteristiques',NULL,'aezr','Chez Tech-no-limit, nous oeuvrons selon des valeurs authentiques qui comptent réellement pour nos partenaires. C’est ce qui dirige notre travail quotidien et ce qui tient notre équipe “tissée serrée”. Le lorem ipsum est, en imprimerie, une suite de mots sans signification utilisée à titre provisoire pour calibrer une mise en page, le texte définitif venant remplacer le faux-texte dès qu\'il est prêt ou que la mise en page est achevée.',NULL,12,NULL,NULL,NULL),(74,'Nos projets',NULL,NULL,NULL,NULL,13,NULL,NULL,NULL),(76,'Nos galeries',NULL,NULL,NULL,NULL,14,NULL,NULL,NULL),(77,'Portable',NULL,'i-5f2bfcc7c0beb.jpeg',NULL,NULL,NULL,76,NULL,NULL),(78,'Portable',NULL,'4o-5f2bfe0c1dbf0.jpeg',NULL,NULL,NULL,76,NULL,NULL),(79,'Lorem ipsum',NULL,'uu-5f2bfe4761a0a.jpeg',NULL,NULL,NULL,76,NULL,NULL),(80,'Lorem ipsum',NULL,'pp-5f2bfe673fc06.jpeg',NULL,NULL,NULL,76,NULL,NULL),(81,'Lorem ipsum','Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','a-5f2c020faa962.jpeg','Lorem ipsum','\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',NULL,74,NULL,NULL),(82,'Lorem ipsum','Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','b-5f2c025133d6d.jpeg','Lorem ipsum','Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',NULL,74,NULL,NULL),(83,'Lorem ipsum','Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','bgi-5f2c0274201e5.jpeg','Lorem ipsum','Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',NULL,74,NULL,NULL),(84,'Lorem ipsum','Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','c-5f2c02dba9da4.jpeg','Lorem ipsum','Exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',NULL,74,NULL,NULL),(87,'test',NULL,'4o-5f2c1d78ad572.jpeg',NULL,NULL,NULL,72,NULL,NULL),(88,NULL,NULL,'i-5f2c1db68a5d3.jpeg',NULL,NULL,NULL,72,NULL,NULL),(90,NULL,NULL,'p-5f2c1e8958ea8.jpeg',NULL,NULL,NULL,72,NULL,NULL),(91,NULL,NULL,'pho-5f2c1e9a03604.jpeg',NULL,NULL,NULL,72,NULL,NULL),(92,'On se soucie des autres',NULL,'thumb-3-5f2c223e43843.gif','Notre environnement collaboratif valorise les compétences complémentaires de l’équipe afin de créer des projets innovants qui vous bénéficieront.\',',NULL,NULL,73,NULL,NULL),(93,'POUR SERVIR',NULL,'marvin-meyer-SYTO3xs06fU-unsplash-5f48b0183be3a.jpeg','A votre entreprise',NULL,NULL,50,NULL,NULL),(94,'SERVIR',NULL,'bram-naus-n8Qb1ZAkK88-unsplash-5f48b34b3de38.jpeg',NULL,NULL,NULL,50,NULL,NULL),(96,'AIDER',NULL,'florian-olivo-4hbJ-eymZ1o-unsplash-5f48b140d8534.jpeg',NULL,NULL,NULL,50,NULL,NULL),(97,'COLLABORER',NULL,'kelly-sikkema-RgXO6LZbZgg-unsplash-5f4652933fc6b.jpeg',NULL,NULL,NULL,50,NULL,NULL),(98,'FACILITER',NULL,'scott-graham-5fNmWej4tAA-unsplash-5f48b1a779968.jpeg',NULL,NULL,NULL,50,NULL,NULL),(99,NULL,NULL,'WordPress-01-5f476de9824bb.png',NULL,NULL,NULL,43,NULL,NULL),(100,NULL,NULL,'React-01-5f476e12e0acb.png',NULL,NULL,NULL,43,NULL,NULL),(101,NULL,NULL,'Symfony-01-5f476e218b3ba.png',NULL,NULL,NULL,43,NULL,NULL),(102,NULL,NULL,'HTML-5-01-5f476e7fa6c49.png',NULL,NULL,NULL,43,NULL,NULL),(103,NULL,NULL,'CSS-3-01-5f476e9eaa913.png',NULL,NULL,NULL,43,NULL,NULL);
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_child`
--

DROP TABLE IF EXISTS `content_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_child` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) DEFAULT NULL,
  `title1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `full_description` longtext COLLATE utf8mb4_unicode_ci,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C97000FB84A0A3ED` (`content_id`),
  CONSTRAINT `FK_C97000FB84A0A3ED` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_child`
--

LOCK TABLES `content_child` WRITE;
/*!40000 ALTER TABLE `content_child` DISABLE KEYS */;
INSERT INTO `content_child` VALUES (1,81,'az','az','az','az','Untitled-1-5f451f5a37d7b.png'),(2,39,'logo angular',NULL,NULL,NULL,'5847ea22cef1014c0b5e4833-5f4ce636dc821.png'),(4,81,'er',NULL,NULL,NULL,NULL),(6,69,NULL,NULL,NULL,NULL,'woo-wordpress-5f29b69561fdf-5f4cdf2dcc0ea.png'),(7,40,NULL,NULL,NULL,NULL,'woo-wordpress-5f29b69561fdf-5f4cdf3d29f6c.png'),(8,39,'logo node js',NULL,NULL,NULL,'Node-JS-01-5f4ce664113ca.png'),(9,39,'logo magento',NULL,NULL,NULL,'Magento-Icon-01-5f4ce86763a65.png'),(10,39,'logo jquerry',NULL,NULL,NULL,'JQuery-01-5f4ce8aba9520.png'),(11,39,'logo symfony',NULL,NULL,NULL,'Symfony-01-5f4ce9fd2afcb.png'),(12,39,'logo mysql',NULL,NULL,NULL,'MySQL-01-5f4cea66a57d7.png'),(13,39,'logo wordpress',NULL,NULL,NULL,'WordPress-01-5f4ceab173d29.png');
/*!40000 ALTER TABLE `content_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estimate_type`
--

DROP TABLE IF EXISTS `estimate_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estimate_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estimate_type`
--

LOCK TABLES `estimate_type` WRITE;
/*!40000 ALTER TABLE `estimate_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `estimate_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estimation`
--

DROP TABLE IF EXISTS `estimation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estimation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estimation_type_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D0527024E8E307CC` (`estimation_type_id`),
  CONSTRAINT `FK_D0527024E8E307CC` FOREIGN KEY (`estimation_type_id`) REFERENCES `estimation_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estimation`
--

LOCK TABLES `estimation` WRITE;
/*!40000 ALTER TABLE `estimation` DISABLE KEYS */;
INSERT INTO `estimation` VALUES (1,1,'Designe',10);
/*!40000 ALTER TABLE `estimation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estimation_type`
--

DROP TABLE IF EXISTS `estimation_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estimation_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estimation_type`
--

LOCK TABLES `estimation_type` WRITE;
/*!40000 ALTER TABLE `estimation_type` DISABLE KEYS */;
INSERT INTO `estimation_type` VALUES (1,'type_web_e_commerce','Site E-commerce',200),(2,'type_web_vitrine','Site vitrine',100),(5,'type_web_mobile_ios','web + ios',300),(6,'type_web_mobile_android','web + android',300),(7,'type_web_mobile_android_ios','web + android + ios',400),(8,'type_mobile_ios','IOS',200),(9,'type_mobile_android','Android',200),(10,'type_mobile_android_ios','Android + ios',300);
/*!40000 ALTER TABLE `estimation_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
INSERT INTO `team` VALUES (5,'RALITERA Idealy Tokiniaina','Chef de projet Informatique','Aime le défi et passionné par la transformation digitale.\r\n\r\n\" Rien est impossible ...\"','ralitera.toky7@gmail.com','https://www.linkedin.com/in/ralitera-idealy-tokiniaina/','toky2-5f3281511a118.png'),(6,'ANDRINIRINA Erick','Développeur Full Stack','Je suis capable de mener un projet web de A à Z en autonomie,que ce soit front ou back','ckandrinirina@gmail.com','https://www.linkedin.com/in/andrinirina-erick-2aa6b0184/','erick-5f4540c09711c.jpeg'),(7,'RABEMIAFARA Claudio','Développeur Front-End','Formateur / Concépteur de site web / Administrateur de base de données / Conception design graphique','c_rabemiaf@gmail.com','https://linkedin.com/in/claudino-rabemiafara-255236135','claudio-2-5f3251a6e445c.jpeg'),(8,'Andry Sitraka RAZAFINIMARO','Développeur Back-End','Spécialiste dans le développement web surtout pour le backend de votre site.','andri_s@gmail.com','https://www.linkedin.com/mwlite/in/andry-sitraka-razafinimaro-624686144','sitraka-5f325483972cb.jpeg'),(9,'Narindra Priscilla','Développeur .NET','Passionée par les technoloiges Microsoft. Je suis prête pour tous vos développements.','nariPr@gmail.com','https://www.linkedin.com/in/priscilla-ralitera/','narindra-2-5f32661f5b8af.jpeg'),(11,'Manjato Nick','Développeur Full Stack','Ambitieux et rigoureux. J\'aime travailler en équipe et veiller pour pouvoir toujours livrer à temps.','manj75@gmail.com','https://www.linkedin.com/in/manjato-nick-505772b5/','manjato-5f3265461ad89.jpeg'),(12,'Nady RALAMBOSON','Développeur BI et gestionnaire de projet','Spécialiste en Business Intelligence, traitement et analyse de donnees (datacleaning, data analysis)','nady88@gmail.com','https://www.linkedin.com/in/nady-ralamboson-878b2a68/','nady-5f3266be07b28.jpeg'),(13,'Zo Mirado','Développeur Java','Rien est ça va que Java ;) Je me suis spécialisé dans cette technologie et c\'est vraiment ma passion.','zomir93@gmaaail.com','https://www.linkedin.com/in/ralitera-zo-mirado-55bb48b3/','zo-5f327c1826b7b.jpeg');
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type`
--

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` VALUES (3,'who_are_we','Accueil/Qui sommes-nous'),(4,'our_comp','Accueil/Notre competence'),(5,'graphic','Création/Graphique'),(7,'web_create','Création/Création site Web'),(8,'techno_use','Création/Création site Web/Les technologies Utilisées'),(9,'experience','Accueil/Nos experiences'),(10,'slide','Navbar/Slide'),(11,'about_slide','A propos/Slide sur à propos de nous'),(12,'about_feature','A propos/caractéristique'),(13,'portfolio','Portfolio/'),(14,'gallery','A propos/Galerie');
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-31 12:20:06
