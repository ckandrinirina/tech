import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeMobileComponent } from './type-mobile.component';

describe('TypeMobileComponent', () => {
  let component: TypeMobileComponent;
  let fixture: ComponentFixture<TypeMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
