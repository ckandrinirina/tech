import { Component, OnInit } from '@angular/core';
import { EstimateService } from 'src/app/service/estimate/estimate.service';

@Component({
  selector: 'app-type-mobile',
  templateUrl: './type-mobile.component.html',
  styleUrls: ['./type-mobile.component.scss','./../estimation.component.scss']
})
export class TypeMobileComponent implements OnInit {

  constructor(public estimateService: EstimateService) { }

  ngOnInit(): void {
  }

}
