import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeWebMobileComponent } from './type-web-mobile.component';

describe('TypeWebMobileComponent', () => {
  let component: TypeWebMobileComponent;
  let fixture: ComponentFixture<TypeWebMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeWebMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeWebMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
