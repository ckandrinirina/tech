import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebTypeComponent } from './web-type.component';

describe('WebTypeComponent', () => {
  let component: WebTypeComponent;
  let fixture: ComponentFixture<WebTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
