import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeWebVitrineComponent } from './type-web-vitrine.component';

describe('TypeWebVitrineComponent', () => {
  let component: TypeWebVitrineComponent;
  let fixture: ComponentFixture<TypeWebVitrineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeWebVitrineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeWebVitrineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
