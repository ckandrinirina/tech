import { Component, OnInit } from '@angular/core';
import { EstimateService } from 'src/app/service/estimate/estimate.service';

@Component({
  selector: 'app-type-web-vitrine',
  templateUrl: './type-web-vitrine.component.html',
  styleUrls: ['./type-web-vitrine.component.scss', './../../estimation.component.scss']
})
export class TypeWebVitrineComponent implements OnInit {

  constructor(public estimationService: EstimateService) { }

  ngOnInit(): void {
    this.estimationService.findEstimationData();
  }

}
