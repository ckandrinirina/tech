import { Component, OnInit } from '@angular/core';
import { EstimateService } from 'src/app/service/estimate/estimate.service';

@Component({
  selector: 'app-estimation-result',
  templateUrl: './estimation-result.component.html',
  styleUrls: ['./estimation-result.component.scss']
})
export class EstimationResultComponent implements OnInit {

  constructor(public estimateService: EstimateService) { }

  ngOnInit(): void {
  }

}
