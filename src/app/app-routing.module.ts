import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { WebCreateComponent } from './web-create/web-create.component';
import { GraphiqueComponent } from './graphique/graphique.component';
import { BlogComponent } from './blog/blog.component';
import { CommerceComponent } from './commerce/commerce.component';
import { EstimationComponent } from './estimation/estimation.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { GalleryComponent } from './gallery/gallery.component';
import { ProcessusComponent } from './processus/processus.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'web-create', component: WebCreateComponent },
  { path: 'graphique', component: GraphiqueComponent },
  { path: 'blog', component: BlogComponent },
  { path: 'commerce', component: CommerceComponent },
  { path: 'estimate', component: EstimationComponent },
  { path: 'about', component: AboutUsComponent },
  { path: 'portfolio', component: PortfolioComponent },
  { path: 'gallery', component: GalleryComponent },
  { path: 'processus', component: ProcessusComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
  bootstrap: [AppComponent]
})
export class AppRoutingModule { }
