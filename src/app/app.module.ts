import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//import module
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatRadioModule } from '@angular/material/radio';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatBadgeModule } from '@angular/material/badge';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatProgressBarModule } from '@angular/material/progress-bar';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ContactComponent } from './contact/contact.component';
import { WhoAreWeComponent } from './home/who-are-we/who-are-we.component';
import { OurCompetenceComponent } from './home/our-competence/our-competence.component';
import { FooterComponent } from './footer/footer.component';
import { ExperienceComponent } from './home/experience/experience.component';
import { InformationComponent } from './home/information/information.component';
import { WebCreateComponent } from './web-create/web-create.component';
import { GraphiqueComponent } from './graphique/graphique.component';
import { CommerceComponent } from './commerce/commerce.component';
import { BlogComponent } from './blog/blog.component';
import { ProductDetailComponent } from './commerce/product-detail/product-detail.component';
import { EstimationComponent } from './estimation/estimation.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LinkAssetsPipe } from './link-assets.pipe';
import { WebTypeComponent } from './estimation/web-type/web-type.component';
import { TypeWebVitrineComponent } from './estimation/web-type/type-web-vitrine/type-web-vitrine.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DesignComponent } from './estimation/design/design.component';
import { LevelComponent } from './estimation/level/level.component';
import { EstimationResultComponent } from './estimation/estimation-result/estimation-result.component';
import { TypeMobileComponent } from './estimation/type-mobile/type-mobile.component';
import { TypeWebMobileComponent } from './estimation/type-web-mobile/type-web-mobile.component';
import { AboutUsComponent, CvSheet } from './about-us/about-us.component';
import { DemandeDevisComponent } from './demande-devis/demande-devis.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { GalleryComponent } from './gallery/gallery.component';
import { MyLoaderComponent } from './components/my-loader/my-loader.component';
import { LoaderService } from './service/loader/loader.service';
import { LoaderInterceptor } from './interceptors/loader-interceptor.service';
import { DialogPortfolioComponent } from './portfolio/dialog-portfolio/dialog-portfolio.component';
import { ProcessusComponent } from './processus/processus.component';
import { IntroComponent } from './home/intro/intro.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

const appRoutes: Routes = [
  { path: 'connexion/oubli-mdp', component: WebCreateComponent },
  { path: 'estimate', component: EstimationComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    ContactComponent,
    WhoAreWeComponent,
    OurCompetenceComponent,
    FooterComponent,
    ExperienceComponent,
    InformationComponent,
    WebCreateComponent,
    GraphiqueComponent,
    CommerceComponent,
    BlogComponent,
    ProductDetailComponent,
    EstimationComponent,
    LinkAssetsPipe,
    WebTypeComponent,
    TypeWebVitrineComponent,
    DesignComponent,
    LevelComponent,
    EstimationResultComponent,
    TypeMobileComponent,
    TypeWebMobileComponent,
    AboutUsComponent,
    DemandeDevisComponent,
    PortfolioComponent,
    GalleryComponent,
    MyLoaderComponent,
    DialogPortfolioComponent,
    CvSheet,
    ProcessusComponent,
    IntroComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
    MatDialogModule,
    HttpClientModule,
    MatToolbarModule,
    MatSlideToggleModule,
    FormsModule,
    MatRadioModule,
    MatTooltipModule,
    MatInputModule,
    MatFormFieldModule,
    MatBadgeModule,
    ReactiveFormsModule,
    MatBottomSheetModule,
    MatProgressBarModule,
    NgbModule
  ],
  providers: [
    LoaderService,
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
