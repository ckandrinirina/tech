import { Component, OnInit } from '@angular/core';
import { CommonFrontService } from 'src/app/service/common/common-front-service';

@Component({
  selector: 'app-who-are-we',
  templateUrl: './who-are-we.component.html',
  styleUrls: ['./who-are-we.component.scss']
})
export class WhoAreWeComponent implements OnInit {
  dataFromDb = {
    who_are_we: {
      titre1: 'QUI SOMMES-NOUS?',
      titre2: '',
      img: '',
      description: 'ghfgkjnhjghj t. Quasi eaque dolores sed rem dolorum reiciendis quae sunt molestias, magnam iure illum architecto tenetur! Voluptatibus voluptatum, libero consequuntur minima repellendus consequatur? hofjlwhflwkj',
      full_description: '',
      child: [
      {
        titre1: 'LOREM IPSUM 1',
        titre2: '',
        img: '',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi eaque dolores sed rem dolorum reiciendis quae sunt molestias, magnam iure illum architecto tenetur! Voluptatibus voluptatum, libero consequuntur minima repellendus consequatur? hofjlwhflwkj',
        full_description: ''
      },
      {
        titre1: 'LOREM IPSUM2',
          titre2: '',
          img: '',
          description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi eaque dolores sed rem dolorum reiciendis quae sunt molestias, magnam iure illum architecto tenetur! Voluptatibus voluptatum, libero consequuntur minima repellendus consequatur? hofjlwhflwkj',
          full_description: ''
        },
      {
        titre1: 'LOREM IPSUM3',
        titre2: '',
        img: '',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi eaque dolores sed rem dolorum reiciendis quae sunt molestias, magnam iure illum architecto tenetur! Voluptatibus voluptatum, libero consequuntur minima repellendus consequatur? hofjlwhflwkj',
        full_description: ''
      }
      ]
    }
  }
  constructor(public commonService: CommonFrontService) { }

  ngOnInit(): void {
  }

  setBarLenth(value:any){
    return "background-color: #003c85; width: "+value+"%;";
  }

}
