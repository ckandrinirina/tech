import { Component, OnInit } from '@angular/core';
import { CommonFrontService } from 'src/app/service/common/common-front-service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
dataFromdb = {
  gallery:{
    title1:'Nos Galeries',
    title2:'',
    description:'',
    fulldescription:'',
    img:'',
    childs: [{
      img:'assets/slide_galerie/images/uu.jpg',
      title1: 'Portable'
    },
  {
    img: 'assets/slide_galerie/images/ii.jpg',
    title1: 'Portable'
  },
  {
    img: 'assets/slide_galerie/images/uu.jpg',
    title1: 'Portable'
  },
  {
    img: 'assets/slide_galerie/images/ii.jpg',
    title1: 'Portable'
  },
  {
    img: 'assets/slide_galerie/images/uu.jpg',
    title1: 'Portable'
  },
      {
        img: 'assets/slide_galerie/images/uu.jpg',
        title1: 'Portable'
      },
      {
        img: 'assets/slide_galerie/images/ii.jpg',
        title1: 'Portable'
      },
      {
        img: 'assets/slide_galerie/images/uu.jpg',
        title1: 'Portable'
      }
]
}
}
  constructor(public commonService:CommonFrontService) { }

  ngOnInit(): void {
  }

}
